#pragma once 
#include <SFML/Graphics.hpp>
#include<iostream>

namespace el
{
	class Item   //предмет
	{
	public:
		Item(int w, int h, int x, int y, double M, double v, float velocity);
		~Item();

		bool Setup();

		sf::Sprite* Get();

		void Move();

		int Status(int RO);

		int GetY();

		void Bobber(int water_lev);//поплавок колыхание предмета через +5 пикселя -5 пикселя

	private:
		int m_x, m_y;
		int m_w, m_h;
		double m_M, m_v;
		int m_velocity;
		sf::Texture m_texture;
		sf::Sprite* m_shape;
	};
}
