#include "exam.hpp"

namespace el
{
	Item::Item(int w, int h, int x, int y, double M, double v, float velocity )  //ширина высота
	{
		m_w = w;
		m_h = h;
		m_x = x;
		m_y = y;
		m_velocity = velocity;

		m_M = M;
		m_v = v;
	}

	bool Item::Setup()
	{
		if (!m_texture.loadFromFile("img/bottle.png"))
		{
			std::cout << "ERROR when loading bottle.png" << std::endl;
			return false;
		}

		m_shape = new sf::Sprite();
		m_shape->setTexture(m_texture);
		m_shape->setScale(0.2,0.2);
		m_shape->setOrigin((m_w / 2), (m_h / 2));
		m_shape->setPosition(m_x, m_y);

		return true;
	}


	Item::~Item()
	{
		delete m_shape;
	}

	sf::Sprite* Item::Get() { return m_shape; }

	void Item::Move()
	{
		m_y += m_velocity;
		m_shape->setPosition(m_x, m_y);
	}

	int Item::Status(int RO)
	{
		if ((m_M - RO * m_v) > 0)
		{
			return 1;
		}
		else if ((m_M - RO * m_v) < 0)
		{
			return 2;
		}
	}

	int Item::GetY() { return m_y; }

	void Item::Bobber(int water_lev)
	{
		if (m_y == water_lev)
			m_y = m_y + 5;
		else if (m_y > water_lev)
			m_y = m_y - 5;
		m_shape->setPosition(m_x, m_y);
	}

}
