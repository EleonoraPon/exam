#include <SFML/Graphics.hpp>
#include <thread>
#include <iostream>
#include <chrono>
#include "exam.hpp"

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")
using namespace std::chrono_literals;

int main()
{
	const int RO = 1030;

    const int water_lev = 110;
    const int sand_lev = 490;

    srand(time(0));

    int width = 846;
    int height = 600;

   

    sf::RenderWindow window(sf::VideoMode(width, height), "BottleBottom");  //тонуть

    sf::Texture texture;                         //фон
    if (!texture.loadFromFile("img/bottom.jpg"))
    {
        std::cout << "ERROR when loading bottom.jpg" << std::endl;
        return false;
    }
    sf::Sprite back;
    back.setTexture(texture);


    el::Item* item = new el::Item(90, 40, 423, water_lev, 1.5, 0.5, 5);
	item->Setup();

    sf::Image icon;
    if (!icon.loadFromFile("img/star.png"))
    {
       return -1;
    }
    window.setIcon(37, 32, icon.getPixelsPtr());


	int status = item->Status(RO);

	int cnt = 0;

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }


		if (status == 1)
		{
			if (item->GetY() < sand_lev)
			{
				item->Move();
			}
		}
		else if (status == 2)
		{
			if(cnt%13==0)
				item->Bobber(water_lev);
			cnt++;
		}


        window.clear();

		window.draw(back);
		window.draw(*item->Get());

        window.display();

		std::this_thread::sleep_for(40ms);
    }
    delete item;
    return 0;
}
